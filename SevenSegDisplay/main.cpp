#include <iostream>
#include <algorithm>
#include <sstream>
#include "main.h"
#include "SevenSegDisplaySample.h"
#include "WiringPiExtensionApi.h"


using namespace std;


//  Map of MCP chips, pin base to number of pins
map<int, int> McpMap;

// Map of Pca chips, pin base to file descriptor
map<int, int> PcaMap;


int GetIntegerFromKeyboard(string message)
{
	string input;
	int number;
	
	while (true)
	{
		cout << message << " ";
		getline(cin, input);

		stringstream convertor;
		

		convertor << input;
		convertor >> number;

		if (convertor.fail())
		{
			// numberString is not a number!
			cout << "Please enter an integer ! " << endl;
		}
		else
			break;
	}
	
	return number;
}	


int main(int argc, char *argv[])
{
	//  setup wiring pi extension (which will init wiringPi library using physical pin numbers)
	int retVal = SetupWiringPiExtension();

	// mcp chip
	Mcp23017Setup(700, 0x22);
	McpMap[700] = 16;

	return SevenSegDisplaySampleMain(argc, argv);
	
	ShutDownWiringPiExtension();
}