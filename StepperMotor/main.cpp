#include <iostream>
#include <sstream>
#include <algorithm>
#include "main.h"
#include "StepperMotorSample.h"
#include "WiringPiExtensionApi.h"

using namespace std;

//  Map of MCP chips, pin base to number of pins
map<int, int> McpMap;

// Map of Pca chips, pin base to file descriptor
map<int, int> PcaMap;


int GetIntegerFromKeyboard(string message)
{
	string input;
	int number;
	
	while (true)
	{
		cout << message << " ";
		getline(cin, input);

		stringstream convertor;
		
		convertor << input;
		convertor >> number;

		if (convertor.fail())
		{
			// numberString is not a number!
			cout << "Please enter an integer ! " << endl;
		}
		else
			break;
	}
	
	return number;
}	


float GetFloatFromKeyboard(string message)
{
	string input;
	float number;
	
	while (true)
	{
		cout << message << " ";
		getline(cin, input);

		stringstream convertor;

		convertor << input;
		convertor >> number;

		if (convertor.fail())
		{
			// numberString is not a number!
			std::cout << "Please enter a number !" << endl;
		}
		else
			break;
	}
	
	return number;
}	



int main(int argc, char *argv[])
{
	//  setup wiring pi extension (which will init wiringPi library using physical pin numbers)
	int retVal = SetupWiringPiExtension();
	
	//  setup MCP expander chips
	//	Mcp23017Setup(100, 0x20);
	//  McpMap[100] = 16
	Mcp23017Setup(200, 0x23);
	McpMap[200] = 16;
	
	Mcp23008Setup(300, 0x24);
	McpMap[300] =  8;
	
	Mcp23008Setup(400, 0x27);
	McpMap[400] =  8;
	
	Mcp23017Setup(700, 0x22);
	McpMap[700] = 16;
	
	//  Adafruit DC Motor Hat at default address
	int fd = Pca9685Setup(600, 0x60, 50.0);
	if (fd != 0) 
	{
		// Reset all output 
		Pca9685PWMReset(fd); 
		PcaMap[600] = fd;
	}
	
	return StepperMotorSampleMain(argc, argv);
}