#include <map>
#include <string.h>
#include "WiringPiExtensionApi.h"
#include "WiringPiExtensionMotorWithRotaryEncoderApi.h"
#include "WiringPiExtensionRotaryEncoderApi.h"
#include "MotorWithRotaryEncoder.h"
#include "Logging.h"
#include "PinHelpers.h"
#include "StringExtensionMethods.h"


using namespace std;

//  Map of motor with encoder index to motor with encoder object
map<int, MotorWithRotaryEncoder*> MotorWithRotaryEncoderMap;


MotorWithRotaryEncoder* FindMotorWithRotaryEncoder(int index)
{
	map<int, MotorWithRotaryEncoder*>::iterator it = MotorWithRotaryEncoderMap.find(index);
	if (it != MotorWithRotaryEncoderMap.end())
	{
		return it->second;
	}
	return NULL;
}


void ShutDownMotorsWithRotaryEncoder()
{
	//  delete all motors with rotary encoder
	map<int, MotorWithRotaryEncoder*>::iterator itMotorsWithEncoders;
	for (itMotorsWithEncoders = MotorWithRotaryEncoderMap.begin(); itMotorsWithEncoders != MotorWithRotaryEncoderMap.end(); ++itMotorsWithEncoders)
	{
		delete itMotorsWithEncoders->second;
	}
	MotorWithRotaryEncoderMap.clear();
}


//  Motor with Rotary Encoder
//
#pragma region MotorWithRotaryEncoder

int MotorWithRotaryEncoderCreate(const int bridgeIn1, const int bridgeIn2, const int bridgePwm, const int encoderA, const int encoderB, const int encoderIndex, const int countsPerRevolution, EncoderUpdatedCallback callback)
{
	int newEncoder = RotaryEncoderCreate(encoderA, encoderB, encoderIndex, countsPerRevolution,callback);
	
	if (newEncoder < 0)
		return newEncoder;	//  error creating the encoder (too many created already)
	
	//  get the next available index number
	int index = 0;
	while (true)
	{
		map<int, MotorWithRotaryEncoder*>::iterator it = MotorWithRotaryEncoderMap.find(index);
		if (it == MotorWithRotaryEncoderMap.end())
		{
			break;
		}
		index++;
	}
	
	MotorWithRotaryEncoder* newMotorWithEncoder = new MotorWithRotaryEncoder(bridgeIn1, bridgeIn2, bridgePwm, GetPwmRangeForPin(bridgePwm), FindRotaryEncoder(newEncoder));
	MotorWithRotaryEncoderMap[index] = newMotorWithEncoder;
	return index;
}


void MotorWithRotaryEncoderRemove(const int index)
{
	MotorWithRotaryEncoder* findMotor = FindMotorWithRotaryEncoder(index);
	if (findMotor != NULL)
	{
		findMotor->Stop();
		MotorWithRotaryEncoderMap.erase(index);
		delete findMotor;
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderRemove", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


void MotorWithRotaryEncoderSetUsefulPowerRange(int index, double minPower, double maxPower)
{
	MotorWithRotaryEncoder* findMotor = FindMotorWithRotaryEncoder(index);
	if (findMotor != NULL)
	{
		findMotor->SetUsefulPowerRange(minPower, maxPower);
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderSetUsefulPowerRange", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


void MotorWithRotaryEncoderResetCount(const int index, int setCount)
{
	MotorWithRotaryEncoder* findMotor = FindMotorWithRotaryEncoder(index);
	if (findMotor != NULL)
	{
		findMotor->ResetCount(setCount);
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderResetCount", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


	
int MotorWithRotaryEncoderGetCount(const int index)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		return motor->GetCount();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderGetCount", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
	
	return 0;
}


	
int MotorWithRotaryEncoderGetTick(const int index)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		return motor->GetTick();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderGetTick", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
	
	return 0;
}



double MotorWithRotaryEncoderGetCircle(const int index)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		return motor->GetCircle();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderGetCircle", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
	
	return 0;
}


double  MotorWithRotaryEncoderGetRpm(int index)
	
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		return motor->GetRpm();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderGetRpm", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
	
	return 0;
}


double  MotorWithRotaryEncoderGetTickFrequency(int index)
	
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		return motor->GetTickFrequency();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderGetTickFrequency", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
	
	return 0;
}


double  MotorWithRotaryEncoderGetFrequency(int index)
	
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		return motor->GetCountFrequency();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderGetFrequency", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
	
	return 0;
}


void MotorWithRotaryEncoderRun(const int index, double power)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		motor->Run(power);
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderRun", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


void MotorWithRotaryEncoderStop(const int index)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		motor->Stop();
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderStop", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


void MotorWithRotaryEncoderBrake(const int index, double power)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		motor->BrakeMotor(power);
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderBrake", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


void MotorWithRotaryEncoderTurnBy(const int index, double rotations, double power)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		motor->TurnBy(rotations, power);
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderTurnBy", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


void MotorWithRotaryEncoderHoldAt(const int index, double circle, double power)
{
	MotorWithRotaryEncoder* motor = FindMotorWithRotaryEncoder(index); 
	if (motor != NULL)
	{
		motor->TurnTo(circle, power);
	}
	else
	{
		Log("WiringPiExtensionRotaryEncoder.cpp", "MotorWithRotaryEncoderHoldAt", format("Error: Unable to find motor %d.", index), LogLevelError);
	}
}


#pragma endregion
