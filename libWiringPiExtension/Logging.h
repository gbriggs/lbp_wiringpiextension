#pragma once

#include "WiringPiExtensionApi.h"

// Logging Callback
extern LoggingCallback LogFunction;

//  Log Level
extern LogLevel LoggingLevel;

//  Add a log to the program logging
void Log(std::string sender, std::string function, std::string data, LogLevel level);