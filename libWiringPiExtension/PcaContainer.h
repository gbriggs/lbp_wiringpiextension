#pragma once

class PcaContainer
{
public:
	PcaContainer(int base, int fd, int freq)
	{
		Base = base;
		Fd = fd;
		Freq = freq;
	}
	
	int GetBase()
	{
		return Base;
	}
	
	int GetFd()
	{
		return Fd;
	}
	
	int GetFreq()
	{
		return Freq;
	}
	
	
protected:
	int Base;
	int Fd;
	int Freq;
};