#include <map>
#include "WiringPiExtensionApi.h"
#include "PcaContainer.h"
#include "pca9685.h"
#include "PinHelpers.h"

using namespace std;

//  Map of pin base number to PCA chip object
map<int, PcaContainer*> PcaChipMap;
//  Map of pin base number to file descriptor
map<int, int> PcaFdMap;

int Pca9685GetFrequencyForPin(const int pin)
{
	//  round to the base pin number
	int n = pin - 16;
	int basePin =	(n + 50) / 100 * 100;
	
	//  see if you have created this one already
	map<int, PcaContainer*>::iterator it = PcaChipMap.find(basePin);
	if (it != PcaChipMap.end())
	{
		return it->second->GetFreq();
	}
	
	return 0;
}


extern int Pca9685GetFileDescriptorForPcaPin(const int pin)
{
	return GetFileDescriptorForPcaPin(pin);
}


//  PCA9865 Driver
//
#pragma region PCS9685


int Pca9685Setup(const int pinBase, const int i2cAddress, float freq)
{
	//  see if you have created this one already
	map<int, PcaContainer*>::iterator it = PcaChipMap.find(pinBase);
	if (it != PcaChipMap.end())
	{
		//  remove this one
		delete it->second;
		PcaChipMap.erase(it);
	}
	
	int fd = pca9685Setup(pinBase, i2cAddress, freq);

	if (fd != 0)
	{
		PcaChipMap[pinBase] = new PcaContainer(pinBase, freq, fd);
		PcaFdMap[pinBase] = fd;
	}
	
	return fd;
}


void Pca9685PWMFreq(int fd, float freq)
{
	pca9685PWMFreq(fd, freq);
}


void Pca9685PWMReset(int fd)
{
	pca9685PWMReset(fd);
}


void Pca9685PWMWrite(int fd, int pin, int on, int off)
{
	pca9685PWMWrite(fd, pin, on, off);
}


void Pca9685PWMRead(int fd, int pin, int *on, int *off)
{
	pca9685PWMRead(fd, pin, on, off);
}


void Pca9685FullOn(int fd, int pin, int tf)
{
	pca9685FullOn(fd, pin, tf);
}


void Pca9685FullOff(int fd, int pin, int tf)
{
	pca9685FullOff(fd, pin, tf);
}


#pragma endregion