#pragma once


enum WiringPiSetupMode
{
	SetupModeDefault,
	SetupModeGpio,
	SetupModeSys,
	SetupModePhys,
	
};

int GetFileDescriptorForPcaPin(int pin);

int GetPwmRangeForPin(int pin);
