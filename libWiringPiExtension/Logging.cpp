#include <string>
#include <sys/time.h>


#include "WiringPiExtensionApi.h"
#include "TimeExtensionMethods.h"
#include "Logging.h"

using namespace std;

LoggingCallback LogFunction = 0x00;
LogLevel LoggingLevel = LogLevelAll;




void Log(string sender, string function, string data, LogLevel level)
{
	if (LogFunction)
	{
		wpeLogEvent logItem;
		logItem.LogUnixTimeMilliseconds = GetUnixTimeMilliseconds();
		logItem.Level = level;
		logItem.Thread = 0;
		logItem.Sender = sender.c_str();
		logItem.Function = function.c_str();
		logItem.Data = data.c_str();
		
		if ( level >= LoggingLevel )
			LogFunction(logItem);
	}
}