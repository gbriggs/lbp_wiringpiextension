#include <map>
#include "WiringPiExtensionApi.h"
#include "mcp23008.h"
#include "mcp23017.h"
#include "mcp3004.h"


using namespace std;

//  Map of MCP chip bin base to number of pins
map<int, int> McpChipMap;

//  Get the pin base for a pin assigned to MCP chip
//
int GetPinBaseForMcpPin(int pin)
{
	//  find the MCP chip for this pin from the map (pin base, number of pins)
	map<int, int>::iterator it;
	for (it = McpChipMap.begin(); it != McpChipMap.end(); it++)
	{
		//  the pin number should be between the base and base + number of pins
		if(pin >= it->first && pin < it->first + it->second)
			return it->first;
	}
	
	return 0;
}


//  mcp23***.h
//
#pragma region MCP23xxx

//  MCP23008 port expansion chip
//
int Mcp23008Setup(int pinBase, int address)
{
	int ret = mcp23008Setup(pinBase, address);
	if (ret == 0)
		McpChipMap[pinBase] = 8;
	
	return ret;
}


//  MCP23017 port expansion chip
//
int Mcp23017Setup(int pinBase, int address)
{
	int ret = mcp23017Setup(pinBase, address);
	if (ret == 0)
		McpChipMap[pinBase] = 16;
	
	return ret;
}

#pragma endregion


//  mcp3004.h
//
#pragma region MCP300x

//  MCP3004 Analog to Digital Converter
//  same code as 3008 but use specific function so we can keep track of pin base and pin number
int Mcp3004Setup(int pinBase, int spiChannel)
{
	int ret = mcp3004Setup(pinBase, spiChannel);
	if (ret != 0)
		McpChipMap[pinBase] = 4;
	
	return ret;
}


//  MCP3008 Analog to Digital Converter
//   same code as 3004 but use specific function so we can keep track of pin base and pin number
int Mcp3008Setup(int pinBase, int spiChannel)
{
	int ret = mcp3004Setup(pinBase, spiChannel);
	if (ret != 0)
		McpChipMap[pinBase] = 8;
	
	return ret;
}

#pragma endregion

