#include <map>
#include <iostream>
#include <wiringPi.h>
#include <mcp23017.h>
#include <mcp23008.h>
#include <mcp3004.h>
#include "pca9685.h"
#include <softPwm.h>
#include <wiringPiSPI.h>
#include <wiringPiI2C.h>
#include "WiringPiExtensionApi.h"
#include "WiringPiExtensionRotaryEncoderApi.h"
#include "WiringPiExtensionSevenSegDisplayApi.h"
#include "WiringPiExtensionStepperMotorApi.h"
#include "WiringPiExtensionMotorWithRotaryEncoderApi.h"
#include "PinHelpers.h"
#include "Logging.h"


using namespace std;

//  TODO - move this to its own file
map<int, int> SoftPwmThreads;

WiringPiSetupMode SetupMode;



void SetLoggingCallback(LoggingCallback function)
{
	LogFunction = function;
}

void SetLoggingLevel(LogLevel level)
{
	LoggingLevel = level;
}


//  Setup
//
#pragma region Setup

int WiringPiSetup()
{
	SetupMode = SetupModeDefault;
	return wiringPiSetup();
}
	

int WiringPiSetupGpio()
{
	SetupMode = SetupModeGpio;
	return wiringPiSetupGpio();
}
	

int WiringPiSetupSys()
{
	SetupMode = SetupModeSys;
	return wiringPiSetupSys();
}


int WiringPiSetupPhys()
{
	SetupMode = SetupModePhys;
	return wiringPiSetupPhys();
}


int SetupWiringPiExtension()
{
	//  TODO remove
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelVerbose", LogLevelVerbose);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelTrace", LogLevelTrace);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelDebug", LogLevelDebug);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelInfo", LogLevelInfo);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelWarn", LogLevelWarn);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelError", LogLevelError);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelFatal", LogLevelFatal);
	Log("WiringPiExtension.cpp", "SetupWiringPiExtension", "LogLevelUser", LogLevelUser);
	
	int ret = WiringPiSetupPhys();
	
	return ret;
}


int SetupWiringPiExtensionGpio()
{
	int ret = WiringPiSetupGpio();
	
	return ret;
}


void ShutDownWiringPiExtension()
{
	//  shut down our soft pwm threads
	map<int, int>::iterator itPwmThreads;
	for (itPwmThreads = SoftPwmThreads.begin(); itPwmThreads != SoftPwmThreads.end(); ++itPwmThreads)
	{
		softPwmStop(itPwmThreads->second);
	}
	//  clean up the map
	SoftPwmThreads.clear();
	
	ShutDownStepperMotors();
	
	ShutDownSevenSegDisplays();
	
	ShutDownEncoders();
	
	ShutDownMotorsWithRotaryEncoder();
	
}

#pragma endregion



//  GPIO Pin Control
//
#pragma region GPIO

void PinMode(int pin, int mode)
{
	pinMode(pin, mode);
}


void PinModeAlt(int pin, int mode)
{
	pinModeAlt(pin, mode);
}


void DigitalWrite(int pin, int value)
{
	digitalWrite(pin, value);
}


void DigitalWriteByte(int value)
{
	digitalWriteByte(value);
}
	

int  DigitalRead(int pin)
{
	return digitalRead(pin);
}


void AnalogWrite(int pin, int value)
{
	analogWrite(pin, value);
}


int  AnalogRead(int pin)
{
	return analogRead(pin);
}
	

void PullUpDnControl(int pin, int pud)
{
	pullUpDnControl(pin, pud);
}


void PwmSetMode(int mode)
{
	pwmSetMode(mode);
}


void PwmWrite(int pin, int value)
{
	pwmWrite(pin, value);
}
	

void PwmSetRange(unsigned int range)
{
	pwmSetRange(range);
}


void PwmSetClock(int divisor)
{
	pwmSetClock(divisor);
}
	
	
void GpioClockSet(int pin, int freq)
{
	gpioClockSet(pin, freq);
}


int  SoftPwmCreate(int pin, int value, int range)	
{
	//  see if you have created this one already
	map<int, int>::iterator it = SoftPwmThreads.find(pin);
	if (it != SoftPwmThreads.end())
	{
		//  shut down the old one
		SoftPwmStop(pin);
		SoftPwmThreads.erase(pin);
	}
	
	// create the new one
	int ret = softPwmCreate(pin, value, range);
	//  remember it
	SoftPwmThreads[pin] = pin;
	
	return ret;
}


void SoftPwmWrite(int pin, int value)	
{
	softPwmWrite(pin, value);
}


void SoftPwmStop(int pin)	
{
	//  clean up the map
	map<int, int>::iterator it = SoftPwmThreads.find(pin);
	if (it != SoftPwmThreads.end())
		SoftPwmThreads.erase(pin);
	
	softPwmStop(pin);
}

#pragma endregion


//  Timing
//
#pragma region Timing

unsigned int Millis()
{
	return millis();
}
	

unsigned int Micros()
{
	return micros();
}


void Delay(unsigned int howLong)
{
	delay(howLong);
}
	

void DelayMicroseconds(unsigned int howLong)
{
	delay(howLong);
}

#pragma endregion


//  Interrupts
//
#pragma region Interrupts

int PiHiPri(int priority)
{
	return piHiPri(priority);
}


int WaitForInterrupt(int pin, int timeout)
{
	return waitForInterrupt(pin, timeout);
}


int WiringPiISR(int pin, int mode, void(*function)())
{
	return wiringPiISR(pin, mode, function);
}
	
#pragma endregion


//  Board Functions
//
#pragma region PiBoard

int PiBoardRev()
{
	return piBoardRev();
}


int WpiPinToGpio(int wPiPin)
{
	return wpiPinToGpio(wPiPin);
}


int PhysPinToGpio(int physPin)
{
	return physPinToGpio(physPin);
}


void SetPadDrive(int group, int value)
{
	setPadDrive(group, value);

}

#pragma endregion


//  SPI
//
#pragma region SPI

int WiringPiSPISetup(int channel, int speed)
{
	return wiringPiSPISetup(channel, speed);
}


int WiringPiSPIDataRW(int channel, unsigned char* data, int len)
{
	return wiringPiSPIDataRW(channel, data, len);
}

#pragma endregion


//  I2C
//
#pragma region I2C

int WiringPiI2CSetup(int devId)
{
	return wiringPiI2CSetup(devId);
}


int WiringPiI2CRead(int fd)
{
	return wiringPiI2CRead(fd);
}


int WiringPiI2CWrite(int fd, int data)
{
	return wiringPiI2CWrite(fd, data);
}


int WiringPiI2CWriteReg8(int fd, int reg, int data)
{
	return wiringPiI2CWriteReg8(fd, reg, data);
}


int WiringPiI2CWriteReg16(int fd, int reg, int data)
{
	return wiringPiI2CWriteReg16(fd, reg, data);
}


int WiringPiI2CReadReg8(int fd, int reg)
{
	return wiringPiI2CReadReg8(fd, reg);
}


int WiringPiI2CReadReg16(int fd, int reg)
{
	return wiringPiI2CReadReg16(fd, reg);
}


#pragma endregion 


