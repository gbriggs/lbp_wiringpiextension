#include <map>
#include <algorithm>
#include "PcaContainer.h"
#include "PinHelpers.h"

using namespace std;

extern map<int, PcaContainer*> PcaChipMap;
extern map<int, int> PcaFdMap;
extern WiringPiSetupMode SetupMode;



int GetFileDescriptorForPcaPin(int pin)
{
	//  find the PCA chip for this pin from the map (pin base, fd)
	map<int, int>::iterator it;
	for (it = PcaFdMap.begin(); it != PcaFdMap.end(); it++)
	{
		//  the pin number should be between the base and base+16
		if(pin >= it->first && pin < it->first + 16)
			return it->second;
	}
	
	return 0;
}



int GetPwmRangeForPin(int pin)
{
	//  check to see if this is the Pi PWM pin
	switch (SetupMode)
	{
		
	case SetupModeDefault:
		break;
	case SetupModeGpio:
		break;
	case SetupModeSys:
		break;
	case SetupModePhys:
		if (pin == 12)
			return 1024;
		break;
	}
	
	//  check to see if this is a PCA pin
	if (GetFileDescriptorForPcaPin(pin) != 0)
	{
		return 4096;
	}
	
	//  this must be software PWM pin
	return 200;
}

