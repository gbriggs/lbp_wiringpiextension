Wiring Pi Extension
-------------------

A C++ library extending Gordon Henderson's wiringPi library (http://wiringpi.com).  

The Wiring Pi Extension provides a single API to access all of the wiringPi library, plus the following extensions:

* Stepper motor driver
* Seven segment display driver
* PCA9685 PWM chip drivers 
 (using Reinhard Sprung's code https://github.com/Reinbert/pca9685)
* Rotary encoder